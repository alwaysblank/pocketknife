# Utilities

A small collection of simple utilities for PHP.

### safeString / sf

This checks a string against a regular expression (it includes a default which only allows string compose of alphanumeric characters and underscores). It accepts a second argument: a different regular expression to be used in place of the default.

It returns the string if it is safe, and boolean 'false' if it isn't.

### cleanString / sc

This returns a string that has been cleaned based on a regular expression (it includes a default which matches only alphanumeric characters and underscores) and a replacement character (which defaults to nothing, i.e. it just removes unsafe characters). It accepts a second argument (the replacement character) and the third argument (the regular expression). Passing "null" to the second or third arguments will cause the function to use the its defaults. 

It will return a cleaned version of the string.

### inspect / i

This returns an object/array dump wrapped in a `<pre>` element so that it will be nicely formatted. If the second argument is `true` (the default) the method will use `print_r`; if it is `false` the method will use `var_dump`. `var_dump` will truncate nested items at a certain level; `print_r` will not. `var_dump` will return information about node types, however, while `print_r` simply returns the values.